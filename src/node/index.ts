#!/usr/bin/env node
/* eslint-disable no-console */
'use strict';

import { NetworkInterfaceInfoIPv4, networkInterfaces } from 'os';
const chalk = require('chalk');
const inquirer = require('inquirer');
const evilScan = require('evilscan');

/**
 * 10.0.0.0 / 8(Range: 10.0.0.0 – 10.255.255.255)
 * 172.16.0.0 / 12(Range: 172.16.0.0 – 172.31.255.255)
 * 192.168.0.0 / 16(Range: 192.168.0.0 – 192.168.255.255)
 */

const localNetworkRanges = [
  ['10.0.0.0', '10.255.255.255'],
  ['172.16.0.0', '172.31.255.255'],
  ['192.168.0.0', '192.168.255.255'],
];

export const activeNetworkInfo = networkInterfaces();

const ipToNum = (ip: string) => {
  return Number(
    ip
      .split('.')
      .map((d) => ('000' + d).substr(-3))
      .join(''),
  );
};

const ipInRange = (ip: string, range: string[]) => {
  const ipToTest = ipToNum(ip);
  const rangeToTest = range.map((rangeIP: string) => ipToNum(rangeIP));
  const isInRange = rangeToTest[0] < ipToTest && ipToTest < rangeToTest[1];
  return isInRange;
};

const localAddresses: NetworkInterfaceInfoIPv4[] = [];

/** Iterate over interfaces */
for (const networkInterface in activeNetworkInfo) {
  if ({}.hasOwnProperty.call(activeNetworkInfo, networkInterface)) {
    /** Iterate over addresses */
    activeNetworkInfo[networkInterface]?.forEach((interfaceInfo) => {
      const isLocalRoutableAddress =
        interfaceInfo.family === 'IPv4' &&
        !interfaceInfo.internal &&
        /** For each non-routeable network range, check if IP is in it */
        localNetworkRanges.some((range) => ipInRange(interfaceInfo.address, range));
      if (isLocalRoutableAddress) {
        localAddresses.push(interfaceInfo);
      }
    });
  }
}

console.log(chalk.green.bold('Local Addresses found:'));
const localCIDR = localAddresses.map((interfaceInfo) => ({ name: interfaceInfo.cidr }));
localAddresses.forEach((interfaceInfo) => console.log(interfaceInfo.cidr));

inquirer
  .prompt([
    {
      type: 'checkbox',
      message: 'Select interfaces',
      name: 'interfaces',
      choices: localCIDR,
      validate: (answer: any) => {
        if (answer.length < 1) {
          return 'You must choose at least one network.';
        }

        return true;
      },
    },
  ])
  .then(async (answers: any) => {
    /** TODO: add switch to select scanning in sequence or parallel */
    await scanNetworks(answers.interfaces);
  });

/** TODO: Use proper typings and get rid of these `any`s ya noob. */
const scanNetworks = async (networks: any[]) => {
  const scanResults = new Promise((resolve) => {
    const detectedCabs: any[] = [];
    networks.forEach((network: string) => {
      console.log(`Scanning ${network}`);
      const options = {
        target: network,
        port: '12749',
        // status: 'TROU', // Timeout, Refused, Open, Unreachable
        status: 'O', // Timeout, Refused, Open, Unreachable
        banner: true,
      };

      new evilScan(options, (instanceErr: any, scan: any) => {
        if (instanceErr) {
          console.log(instanceErr);
          return;
        }

        scan.on('result', (data: any) => {
          // fired when item is matching options
          console.log(`Possible cabinet detected at ${JSON.stringify(data.ip)}`);
          detectedCabs.push(data);
        });

        scan.on('error', (err: any) => {
          throw new Error(err.toString());
        });

        scan.on('done', () => {
          // finished !
          console.log(`Scan of ${network} complete!`);
          resolve(detectedCabs);
        });

        scan.run();
      });
    });
  });
  return scanResults;
};
