/* eslint-disable no-console */
export const greeter: (name: string) => string = (name) => `Hello ${name}`;

// const WebSocket = require("ws");
interface Cabinet {
  socket: WebSocket;
}

/**
 * Construct socket url
 *
 * Protocol default : ws
 * IP : from local network
 * Port Default : 12749
 */
// const cabinetUrl = `${protocol}://${ip}:${port}`;
const cabinetUrl = `ws://10.15.10.169:12749`;
const cabinet: Cabinet = {
  socket: new WebSocket(cabinetUrl),
};

// Websocket event listeners
const handleCabinetOpen = ($event: Event) => {
  console.log(`${new Date().toTimeString()} Cabinet connected: `, $event);
};
const handleCabinetClose = ($event: Event) => {
  console.log(`${new Date().toTimeString()} Cabinet disconnect: : `, $event);
};
const handleCabinetError = ($event: Event) => {
  console.log(`${new Date().toTimeString()} Cabinet error: `, $event);
};

const handleCabinetMessage = ($event: Event) => {
  console.log(`${new Date().toTimeString()} Cabinet message: `, $event);

  const aliveMessage = '![k[im alive],v[null]]!';
  cabinet.socket.send(aliveMessage);
  //   const parsedMessage = Event.toString().match(/!\[k\[(.+)\],v\[(.*)?\]\]!/);

  //   if (parsedMessage[1] == "alive") {
  //     if (socket.readyState === WebSocket.OPEN) {
  //       const aliveMessage = "![k[im alive],v[null]]!";
  //       socket.send(aliveMessage);
  //     }
  //   }
};
cabinet.socket.onopen = handleCabinetOpen;
cabinet.socket.onclose = handleCabinetClose;
cabinet.socket.onerror = handleCabinetError;
cabinet.socket.onmessage = handleCabinetMessage;

// function setCabinetEvents(cabinet:Cabinet) {
//     let { id, config, socket } = cabinet
//     const {name, code, token} = config

//     if (socket !== null) {

//       socket.on("open", () => {
//         if (socket.readyState === WebSocket.OPEN) {
//           console.log(
//             `${new Date().getDate} Cabinet ${id} connected`
//           );

//         //   _.each(servers, (server) => {
//         //     const {socket} = server

//         //     if (socket && socket.readyState === WebSocket.OPEN) {
//         //       const message = createMessage("cabinetOnline", [
//         //         name, code, token
//         //       ], cabinet, server);

//         //       socket.send(message);
//         //     }
//         //   })
//         }
//       });

//       socket.on("message", message => {
//         const parsedMessage = message.match(/!\[k\[(.+)\],v\[(.*)?\]\]!/);

//         if (parsedMessage[1] == "alive") {
//           if (socket.readyState === WebSocket.OPEN) {
//             const aliveMessage = "![k[im alive],v[null]]!";
//             socket.send(aliveMessage);
//           }
//         } else {
//           _.each(servers, (server) => {
//             const {config, socket} = server
//             const {debug} = config

//             if (socket && socket.readyState === WebSocket.OPEN) {
//               const message = createMessage(parsedMessage[1], parsedMessage[2].split(","), cabinet, server)

//               if (debug) {
//                 socket.send(message);
//               } else {
//                 socket.send(`${moment.now()} = ${message}`);
//               }
//             }
//           })
//         }
//       });

//       socket.on("close", () => {
//         console.log(
//           `${moment().format("llll")} Cabinet ${id} disconnected`
//         );

//         _.each(servers, (server) => {
//           const {socket} = server

//           if (socket && socket.readyState === WebSocket.OPEN) {
//             const message = createMessage("cabinetOffline", [
//               name, code, token
//             ], cabinet, server);

//             socket.send(message);
//           }
//         })
//       });

//       socket.on("error", err => {
//         console.log(
//           `${moment().format("llll")} Cabinet ${id} error`,
//           err
//         );
//       });
//     }
//   }
