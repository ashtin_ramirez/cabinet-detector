# README #

### What is this repository for? ###
This package allows you to quickly select local networks to scan for any ip addresses that are potentially [killer queen cabinets](http://killerqueenarcade.com/).

### How do I get set up? ###
Ensure Node.js / NPM is installed on your system.

### Usage? ###
`npx @killer-queen-arcade/cabinet-detector`