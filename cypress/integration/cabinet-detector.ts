/// <reference types="cypress" />
import { greeter } from "../../lib";

describe('Cabinet Detector', () => {
    beforeEach(() => {
    })
    it('Produces the right greeting', () => {
        const greeting = greeter('Ash')
        cy.wrap(greeting).should('include','Ash')
    })
  })
  